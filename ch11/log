#!/usr/bin/perl

# Converts a number from one logarithmic base to another.
#
# Example: logconvert(8, 2, 5) is 125 because
#          log (base 2) of 8 equals log (base 5) of 125.

sub logconvert {
    my ($number, $inbase, $outbase) = @_;
    return $outbase ** logbase($number, $inbase);
}

print logconvert(8, 2, 5),    "\n"; # 8  in base 2  is what in base 5?
print logconvert(10, 2, 4),   "\n"; # 10 in base 2  is what in base 4?
print logconvert(2, 10, 100), "\n"; # 2  in base 10 is what in base 100?

# logbase($number, $base) computes the logarithm of number in base $base.
#
# Example: logbase(243, 3) is 5, because 3 ** 5 is 243.
#
sub logbase {
    my ($number, $base) = @_;
    return if $number <= 0 or $base <= 0 or $base == 1;
    return log($number) / log($base);
}

